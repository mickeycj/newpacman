var GameLayer = cc.LayerColor.extend({
    init: function() {
        this._super( new cc.Color( 0, 0, 0, 0 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
        
        this.initSprites();
        this.maze.addChild( this.maze.gameStart );
        cc.audioEngine.playMusic( 'res/sounds/intro.wav', true );
        this.addKeyboardHandlers();
        
        this.switchMap = 0;
        
        this.isPlaying = false;
        
        return true;
    },
    
    initSprites: function() {
        if ( this.switchMap == 0 ) {
            this.maze = new Maze( Maze.MAPS.FIRST );
            this.switchMap = 1;
        } else if ( this.switchMap == 1 ) {
            this.maze = new Maze( Maze.MAPS.SECOND );
            this.switchMap = 2;
        } else {
            this.maze = new Maze( Maze.MAPS.THIRD );
            this.switchMap = 0;
        }
        this.maze.setPosition( cc.p( 0, 40 ) );
        this.addChild( this.maze );
        
        this.pacman = new Pacman( 60, 260 );
        this.maze.addChild( this.pacman );
        this.pacman.setMaze( this.maze );
        
        this.enemy1 = new Enemy( 340 , 460 );
        this.maze.addChild( this.enemy1 );
        this.enemy1.setMaze( this.maze );
        this.pacman.setEnemy1( this.enemy1 );
        
        this.enemy2 = new Enemy( 300 , 60 );
        this.maze.addChild( this.enemy2 );
        this.enemy2.setMaze( this.maze );
        this.pacman.setEnemy2( this.enemy2 );
        
        this.enemy3 = new Enemy( 700 , 60 );
        this.maze.addChild( this.enemy3 );
        this.enemy3.setMaze( this.maze );
        this.pacman.setEnemy3( this.enemy3 );
        
        this.enemy4 = new Enemy( 700 , 460 );
        this.maze.addChild( this.enemy4 );
        this.enemy4.setMaze( this.maze );
        this.pacman.setEnemy4( this.enemy4 );
        
        if ( this.isPlaying ) {
            this.pacman.scheduleUpdateWithPriority( 1 );
            this.enemy1.scheduleUpdate();
            this.enemy2.scheduleUpdate();
            this.enemy3.scheduleUpdate();
            this.enemy4.scheduleUpdate();
        }
    },
    
    addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( keyCode, event ) {
                self.onKeyDown( keyCode, event );
            },
            onKeyReleased: function( keyCode, event ) {
                self.onKeyUp( keyCode, event );
            }
        }, this);
    },
 
    onKeyDown: function( keyCode, event ) {
        switch( keyCode ) {
            case cc.KEY.left:
                this.pacman.setCommand( Pacman.DIR.LEFT );
                break;
            case cc.KEY.right:
                this.pacman.setCommand( Pacman.DIR.RIGHT );
                break;
            case cc.KEY.up:
                this.pacman.setCommand( Pacman.DIR.UP );
                break;
            case cc.KEY.down:
                this.pacman.setCommand( Pacman.DIR.DOWN );
                break;
            case cc.KEY.a:
                this.pacman.setCommand( Pacman.DIR.LEFT );
                break;
            case cc.KEY.d:
                this.pacman.setCommand( Pacman.DIR.RIGHT );
                break;
            case cc.KEY.w:
                this.pacman.setCommand( Pacman.DIR.UP );
                break;
            case cc.KEY.s:
                this.pacman.setCommand( Pacman.DIR.DOWN );
                break;
            case cc.KEY.space:
                if ( !this.maze.isPlaying ) {
                    this.maze.restart();
                    this.removeChild( this.maze );
                    this.initSprites();
                } else if ( !this.isPlaying ) {
                    this.isPlaying = true;
                    this.pacman.scheduleUpdateWithPriority( 1 );
                    this.enemy1.scheduleUpdate();
                    this.enemy2.scheduleUpdate();
                    this.enemy3.scheduleUpdate();
                    this.enemy4.scheduleUpdate();
                    this.maze.removeChild( this.maze.gameStart );
                    cc.audioEngine.playMusic( 'res/sounds/intro.wav', false );
                    cc.audioEngine.playMusic( 'res/sounds/chomp.wav', true );
                }
                break;
        }
    },
 
    onKeyUp: function( keyCode, event ) {
        this.pacman.setCommand( Pacman.DIR.STILL );
    }
});

var StartScene = cc.Scene.extend({
    onEnter: function() {
        this._super();
        var layer = new GameLayer();
        layer.init();
        this.addChild( layer );
    }
});

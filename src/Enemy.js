var Enemy = cc.Sprite.extend({
    ctor: function( x, y ) {
        this._super();
        this.initWithFile( 'res/images/enemy.png' );
        
        this.x = x;
        this.y = y;
        this.updatePosition();
        
        this.nextDirection = Enemy.DIR.STILL;
        this.direction = Enemy.DIR.STILL;
    },
    
    setMaze: function( maze ) {
        this.maze = maze;
    },
    
    updatePosition: function() {
        this.setPosition( cc.p( this.x, this.y ) );
    },
    
    update: function( dt ) {
        this.randomMove();
        this.move();
        this.updatePosition();
    },
    
    randomMove: function() {
        if ( this.nextDirection == Enemy.DIR.STILL ) {
            while ( !this.isMovable( this.command ) ) {
                this.command = parseInt( 1 + Math.random() * 4 );
            }
            this.nextDirection = this.command;
        }
        if ( ( this.x - 60 ) % 40 == 0 && ( this.y - 60 ) % 40 == 0 ) {
            this.findNextMove();
        }
        if ( !this.isMovable( this.nextDirection ) ) {
            this.nextDirection = Enemy.DIR.STILL;
        }
        this.direction = this.nextDirection;
    },
    
    move: function() {
      switch ( this.direction ) {
            case Enemy.DIR.UP:
                this.y += Enemy.MOVE_STEP;
                break;
            case Enemy.DIR.DOWN:
                this.y -= Enemy.MOVE_STEP;
                break;
            case Enemy.DIR.LEFT:
                this.x -= Enemy.MOVE_STEP;
                break;
            case Enemy.DIR.RIGHT:
                this.x += Enemy.MOVE_STEP;
                break;
        }  
    },
    
    isMovable: function( dir ) {
        if ( dir == Enemy.DIR.UP ) {
            return !this.maze.isWall( parseInt( ( this.x - 60 ) / 40 + 1 ), parseInt( ( this.y - 60 ) / 40 + 1 ) );
        } else if ( dir == Enemy.DIR.RIGHT ) {
            return !this.maze.isWall( parseInt( ( this.x - 60 ) / 40 ) + 2, parseInt( ( this.y - 60 ) / 40 ) );
        } else if ( dir == Enemy.DIR.LEFT ) {
            return !this.maze.isWall( parseInt( ( this.x - 65 ) / 40 + 1 ), parseInt( ( this.y - 60 ) / 40 ) );
        } else if ( dir == Enemy.DIR.DOWN ) {
            return !this.maze.isWall( parseInt( ( this.x - 60 ) / 40 + 1 ), parseInt( ( this.y - 65 ) / 40 ) ) && this.y != 60 ;
        }
        return true;
    },
    
    findNextMove: function() {
        for ( i = 0; i <= 4; i++ ) {
            if ( parseInt( Math.random() * 55) > 50 ) {
                switch ( i ) {
                    case Enemy.DIR.UP:
                        if ( this.nextDirection == Enemy.DIR.DOWN ) {
                            i = Enemy.DIR.DOWN;
                        }
                        break;
                    case Enemy.DIR.DOWN:
                        if ( this.nextDirection == Enemy.DIR.UP ) {
                            i = Enemy.DIR.UP;
                        }
                        break;
                    case Enemy.DIR.LEFT:
                        if ( this.nextDirection == Enemy.DIR.RIGHT ) {
                            i = Enemy.DIR.RIGHT;
                        }
                        break;
                    case Enemy.DIR.RIGHT:
                        if ( this.nextDirection == Enemy.DIR.LEFT ) {
                            i = Enemy.DIR.LEFT;
                        }
                        break;
                }
                this.nextDirection = i;
                break;
            }
        }
    }
 });

Enemy.MOVE_STEP = 5;
Enemy.DIR = {
    LEFT: 1,
    RIGHT: 2,
    UP: 3,
    DOWN: 4,
    STILL: 0
};

var command = Enemy.DIR.STILL;
var Maze = cc.Node.extend({
    ctor: function( map ) {
        this._super();
        this.WIDTH = 20;
        this.HEIGHT = 13;
        this.MAP = map;
        
        for ( var r = 0; r < this.HEIGHT; r++ ) {
            for ( var c = 0; c < this.WIDTH; c++ ) {
                var s = null;
                if ( this.MAP[ r ][ c ] == '#' ) {
                    s = cc.Sprite.create( 'res/images/wall.png' );
                } else if ( this.MAP[ r ][ c ] == 'S' ) {
                    var s = cc.Sprite.create( 'res/images/start.png' );
                } else if ( this.MAP[ r ][ c ] == 'F' ) {
                    s = cc.Sprite.create( 'res/images/finish.png' );
                } else {
                    s = cc.Sprite.create( 'res/images/floor.png' );
                }
                s.setAnchorPoint( cc.p( 0, 0 ) );
                s.setPosition( cc.p( c * 40, (this.HEIGHT - r - 1) * 40 ) );
                this.addChild( s );
            }
        }
        
        this.gameStart = new GameStart();
        this.gameStart.setPosition( cc.p( 400, 260 ) );
        
        this.gameOver = new GameOver();
        this.gameOver.setPosition( cc.p( 400, 260 ) );
        
        this.isPlaying = true;
    },
    
    isWall: function( x, y ) {
        var r = this.HEIGHT - y - 2;
        var c = x;
        return this.MAP[ r ][ c ] == '#';
    },
    
    endGame: function() {
        cc.audioEngine.playMusic( 'res/sounds/chomp.wav', false );
        this.addChild( this.gameOver );
        this.isPlaying = false;
    },
    
    restart: function() {
        cc.audioEngine.playMusic( 'res/sounds/chomp.wav', true );
        this.removeChild( this.gameOver );
        this.isPlaying = true;
    }
});

Maze.MAPS = {
    FIRST: [
        '####################',
        '##.....#..........##',
        '##.##.####.###.##.##',
        '##.#.........#....##',
        '##...#.#####.###.###',
        '###.##..#.........##',
        '#S...##.#.####.##.F#',
        '##.#......#.....#.##',
        '##.#.##.###.###.#.##',
        '##...#.......#....##',
        '##.#.#.##.####.##.##',
        '##.#....#.........##',
        '####################'
    ],
    SECOND: [
        '####################',
        '##.....#..........##',
        '##.#.#####.######.##',
        '####..........#...##',
        '##...##.####.##.####',
        '##.###..#.....#...##',
        '#S.#...##.###.#.#.F#',
        '##...#....#.....#.##',
        '####.##.######.##.##',
        '##.#......#.......##',
        '##.#.#.##.###.######',
        '##...#..#.........##',
        '####################'
    ],
    THIRD: [
        '####################',
        '##..........#.....##',
        '##..##.####.#####.##',
        '##.##.......#.....##',
        '##....#.###.#.#.#.##',
        '##.#.##.#.....#.#.##',
        '#S.#....#.###.#...F#',
        '####.##...#...#.####',
        '##.#.##.####.####.##',
        '##......#.........##',
        '##.#.#.##.###.######',
        '##.#.#....#.......##',
        '####################'
    ]
};
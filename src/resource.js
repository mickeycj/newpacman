var res = {
    wall_png: 'res/images/wall.png',
    floor_png: 'res/images/floor.png',
    start_png: 'res/images/start.png',
    finish_png: 'res/images/finish.png',
    pacman_png: 'res/images/pacman.png',
    enemy_png: 'res/images/enemy.png',
    startscene_png: 'res/images/startscene.png',
    restartscene_png: 'res/images/restartscene.png',
    intro_sound: 'res/sounds/intro.wav',
    chomp_sound: 'res/sounds/chomp.wav',
    death_sound: 'res/sounds/death.wav',
    victory_sound: 'res/sounds/victory.wav'
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}

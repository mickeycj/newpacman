var Pacman = cc.Sprite.extend({
    ctor: function( x, y ) {
        this._super();
        this.initWithFile( 'res/images/pacman.png' );
        
        Pacman.MOVE_STEP = 5;
 
        this.x = x;
        this.y = y;
        this.updatePosition();
        
        this.nextDirection = Pacman.DIR.STILL;
        this.direction = Pacman.DIR.STILL;
        this.command = Pacman.DIR.STILL;
    },
    
    setMaze: function( maze ) {
        this.maze = maze;
    },
    
    setEnemy1: function( enemy ) {
        this.enemy1 = enemy;
    },
    
    setEnemy2: function( enemy ) {
        this.enemy2 = enemy;
    },
    
    setEnemy3: function( enemy ) {
        this.enemy3 = enemy;
    },
    
    setEnemy4: function( enemy ) {
        this.enemy4 = enemy;
    },
 
    updatePosition: function() {
        this.setPosition( cc.p( this.x, this.y ) );
    },
    
    update: function() {
        if ( this.isAlive() && !this.isFinished() ) {
            if ( !this.isMovable( this.nextDirection ) ) {
                this.nextDirection = Pacman.DIR.STILL;
            }
            this.setDirection();
            this.move();
            this.updatePosition();
        } else {
            this.maze.endGame();
            this.unscheduleUpdate();
            this.enemy1.unscheduleUpdate();
            this.enemy2.unscheduleUpdate();
            this.enemy3.unscheduleUpdate();
            this.enemy4.unscheduleUpdate();
            if ( !this.isAlive() ) {
                cc.audioEngine.playEffect( 'res/sounds/death.wav' );
            } else {
                cc.audioEngine.playEffect( 'res/sounds/victory.wav' );
            }
        }
    },
    
    setCommand: function( cmd ) {
        this.command = cmd;
    },
    
    setDirection: function() {
        if( this.isMovable( this.command ) && ( this.x - 60 ) % 40 == 0 && ( this.y - 60 ) % 40 == 0) {
            this.nextDirection = this.command;
        }
        this.direction = this.nextDirection;
    },
    
    move: function() {
      switch ( this.direction ) {
            case Pacman.DIR.UP:
                this.setRotation( -90 );
                this.y += Pacman.MOVE_STEP;
                break;
            case Pacman.DIR.DOWN:
                this.setRotation( 90 );
                this.y -= Pacman.MOVE_STEP;
                break;
            case Pacman.DIR.LEFT:
                this.setRotation( -180 );
                this.x -= Pacman.MOVE_STEP;
                break;
            case Pacman.DIR.RIGHT:
                this.setRotation( 0 );
                this.x += Pacman.MOVE_STEP;
                break;
        }  
    },
    
    isMovable: function( dir ) {
        if ( dir == Pacman.DIR.UP ) {
            return !this.maze.isWall( parseInt( ( this.x - 60 ) / 40 + 1 ), parseInt( ( this.y - 60 ) / 40 + 1 ) );
        } else if ( dir == Pacman.DIR.RIGHT ) {
            return !this.maze.isWall( parseInt( ( this.x - 60 ) / 40 ) + 2, parseInt( ( this.y - 60 ) / 40 ) );
        } else if ( dir == Pacman.DIR.LEFT ) {
            return !this.maze.isWall( parseInt( ( this.x - 65 ) / 40 + 1 ), parseInt( ( this.y - 60 ) / 40 ) );
        } else if ( dir == Pacman.DIR.DOWN ) {
            return !this.maze.isWall( parseInt( ( this.x - 60 ) / 40 + 1 ), parseInt( ( this.y - 65 ) / 40 ) ) && this.y != 60 ;
        }
        return true;
    },
    
    isAlive: function() {
        return !this.closeTo( this.enemy1 ) && !this.closeTo( this.enemy2 ) && !this.closeTo( this.enemy3 ) && !this.closeTo( this.enemy4 );
    },
    
    closeTo: function( obj ) {
        return ( ( Math.abs( this.x - obj.x ) <= 20 ) && ( Math.abs( this.y - obj.y  ) <= 20 ) );
    },
    
    isFinished: function() {
        return this.x == 740 && this.y == 260;  
    }
});

Pacman.DIR = {
    LEFT: 1,
    RIGHT: 2,
    UP: 3,
    DOWN: 4,
    STILL: 0
};